package ddw_hw1;

import java.util.Scanner;

/*
 * MI-DDW LS 2014/2014 Paralelka 104 
 * Author: Petra Kubernatova (kuberpe2)
 * Date: 24.3.2015
 * 
 * */

/* Gets the URL from user input */
public class URLScanner {
    public String getURL() {
        System.out.println("Enter URL to examine (including http://): ");
        String url;

        Scanner scanIn = new Scanner(System.in);
        url = scanIn.nextLine();
        scanIn.close();

        return url;
    }
}
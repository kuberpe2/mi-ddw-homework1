package ddw_hw1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

/*
 * MI-DDW LS 2014/2014 Paralelka 104 
 * Author: Petra Kubernatova (kuberpe2)
 * Date: 24.3.2015
 * 
 * */

public class Main {

   public static void main(String[] args) {

       /* Scan user input URL */
       URLScanner urlScan = new URLScanner();
       String inputURL = urlScan.getURL();

       try {
    	   /* Connect to URL */
           Document doc = Jsoup.connect(inputURL).get();
           System.out.println("Getting: " + inputURL);

           /* Retrieve relevant content (<body> content)*/
           Elements content = doc.select("body");

           /* Initialize Gate */
           GateClient client = new GateClient();
           
           /* Make a document from the retrieved content */
           client.setDocument(content.get(0).text());
           client.run();

       } catch (IOException e) {
           e.printStackTrace();
       }
   }
}
package ddw_hw1;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * MI-DDW LS 2014/2014 Paralelka 104 
 * Author: Petra Kubernatova (kuberpe2)
 * Date: 24.3.2015
 * 
 * */

public class GateClient {

    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;

    // whether the GATE is initialised
    private static boolean isGateInitilised = false;

    // document to process
    private Document document = null;

    GateClient() {
        initGate();
    }
    
    /* Function for setting the document to be annotated (the one from user input URL)*/
    public void setDocument(String doc) {
        try {
            this.document = Factory.newDocument(doc);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /* Function for retrieving response from Countries REST - http://restcountries.eu/ (converts it into text, which is going to be parsed)*/
    private static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null)
	            reader.close();
	    }
	}
    
    /* Parsing the population info from Countries REST - http://restcountries.eu/ response */
    private String parsePopulation(String response) {
    	String tmp = response.split("population")[1];
		String tmp2 = tmp.split(":")[1];
		String population = tmp2.split(",")[0];
		return population;
	}
    
    /* Parsing the capital city info from Countries REST - http://restcountries.eu/ response */
    private String parseCapital(String response){
		String tmp5 = response.split("capital")[1];
		String capital = tmp5.split("\"")[2];
		return capital;    	
    }
    
    /* Parsing the area info from Countries REST - http://restcountries.eu/ response */
    private String parseArea(String response){
		String tmp3 = response.split("area")[1];
		String tmp4 = tmp3.split(":")[1];
		String area = tmp4.split(",")[0];
		return area;    	
    }
    
    

    public void run() {
        if (!isGateInitilised) {
            initGate();
        }

        try {
        	/* Registering the ANNIE Gazeteer */
            ProcessingResource gazetteerPR = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");

            
            /* Locating the grammar file */
            File japeOrigFile = new File("src/ddw_hw1/grammar.jape");
            java.net.URI japeURI = japeOrigFile.toURI();

            /* Creating feature map for the transducer */
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }

            /* Registering JAPE Transducer */
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            /* Creating corpus pipeline */
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            /* Adding processing resources to our pipeline */
            annotationPipeline.add(gazetteerPR);
            annotationPipeline.add(japeTransducerPR);

            /* Creating a corpus and adding our document (content from user input URL) */
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);

            /* Set the pipeline to the corpus */
            annotationPipeline.setCorpus(corpus);

            /* Run the pipeline */
            annotationPipeline.execute();

            /* Go through documents in the corpus */
            for(int i=0; i< corpus.size(); i++){
 
                Document doc = corpus.get(i);
 
                /* Getting default annotation set */
                AnnotationSet as_default = doc.getAnnotations();
 
                FeatureMap futureMap = null;
                
                /* Getting all token annotations and their total number*/
                AnnotationSet annSetTokens = as_default.get("Country",futureMap);
                System.out.println("************************************************************");
                System.out.println("Total number of Country annotations: " + annSetTokens.size());
                System.out.println("************************************************************");
                System.out.println("Details:");
 
                ArrayList tokenAnnotations = new ArrayList(annSetTokens);
                
                List<String> list = new ArrayList<String>();
                
                /* When no annotations are found */
                if(tokenAnnotations.size()==0){System.out.println("No annotations were found.");}                
                
                /* Looping through country annotations */
                for(int j = 0; j < tokenAnnotations.size(); ++j) {
 
                    /* Getting annotation token */
                    Annotation token = (Annotation)tokenAnnotations.get(j);
 
                    /* Getting the underlying string (the name of the country) from the token */
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    
                    /* Adding the underlying string to a list so we could count occurences */
                    list.add(underlyingString);

 
                }
                
                /* Finding the number of occurences of a country using a Set with unique records */
                Set<String> unique = new HashSet<String>(list);
                for (String key : unique) {
                	System.out.println("=================================");
                	System.out.println(key);
                	System.out.println("=================================");
                	/* Finds number of occurences of a key (the country name) within a collection (our list of underlying strings) */
                	System.out.println("Number of occurences: " + Collections.frequency(list, key));
                	try {
                		/* Sending request to Country REST, getting a response and parsing it into the information we need */
						String response = readUrl("http://restcountries.eu/rest/v1/name/"+ key.toLowerCase() + "?fullText=true");
						System.out.println("Capital city: " + parseCapital(response));
						System.out.println("Population: " + parsePopulation(response));
						System.out.println("Area: " + parseArea(response));
					} catch (Exception e) {
						/* Case when the country can't be found within the Countries REST database. Cases such as Latin America, U.S. ....*/
						System.out.println("Details could not be found in Countries REST.");
					}
            		

                }
             	
 
                
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initGate() {
        try {
            // set GATE home folder
            File gateHomeFile = new File("C:\\Program Files (x86)\\GATE_Developer_8.0");
            Gate.setGateHome(gateHomeFile);

            // set GATE plugins folder
            File pluginsHome = new File("C:\\Program Files (x86)\\GATE_Developer_8.0\\plugins");
            Gate.setPluginsHome(pluginsHome);

            // initialise the GATE library
            Gate.init();

            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);

            // flag that GATE was successfully initialised
            isGateInitilised = true;
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
